package tk.florian7843.kasa_java_api;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


class CryptTest {

  private static final Base64.Encoder ENCODER = Base64.getEncoder();
  private static final Base64.Decoder DECODER = Base64.getDecoder();

  private static Stream<Arguments> getDataSet() {
    return Stream.of(
        Arguments.of("{\"system\":{\"set_relay_state\":{\"state\":1}}}", "AAAAKtDygfiL/5r31e+UtsWg1Iv5nPCR6LfEsNGlwOLYo4HyhueT9tTu36Lfog=="),
        Arguments.of("{\"system\":{\"get_sysinfo\":{}}}", "AAAAHdDygfiL/5r31e+UttG0wJ/sleaP4YfoyvCL9ov2")
        // TODO: Add for more variety.
    );
  }

  @ParameterizedTest
  @MethodSource("getDataSet")
  void encryptToString_1(String input, String expected) {
    assertThat(Crypt.encryptToString(input, true))
        .isEqualTo(new String(DECODER.decode(expected)));
  }

  @ParameterizedTest
  @MethodSource("getDataSet")
  void encryptToString_2(String input, String expected) {
    assertThat(Crypt.encryptToString(input.getBytes(), true))
        .isEqualTo(new String(DECODER.decode(expected)));
  }

  @ParameterizedTest
  @MethodSource("getDataSet")
  void encryptToBytes_1(String input, String expected) {
    assertThat(ENCODER.encodeToString(Crypt.encryptToBytes(input, true)))
        .isEqualTo(expected);
  }

  @ParameterizedTest
  @MethodSource("getDataSet")
  void encryptToBytes_2(String input, String expected) {
    assertThat(ENCODER.encodeToString(Crypt.encryptToBytes(input.getBytes(), true)))
        .isEqualTo(expected);
  }

  @ParameterizedTest
  @MethodSource("getDataSet")
  void decryptToString(String expected, String input) {
    ByteBuffer encryptedMessage = ByteBuffer.wrap(DECODER.decode(input));

    // split encryptedMessage into message size and bytes, to remove TCP length header
    int messageSize = encryptedMessage.getInt();
    byte[] messageBytes = new byte[messageSize];

    for (int i = 0; i < messageSize; i++) {
      messageBytes[i] = encryptedMessage.get();
    }

    assertThat(Crypt.decryptToString(messageBytes))
        .isEqualTo(expected);
  }
}