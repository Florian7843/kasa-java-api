package tk.florian7843.kasa_java_api;

import java.nio.ByteBuffer;

public class Crypt {

  private static final int START_KEY = 0xAB;

  public static String encryptToString(String message, boolean addLengthHeader) {
    return encryptToString(message.getBytes(), addLengthHeader);
  }

  public static String encryptToString(byte[] message, boolean addLengthHeader) {
    return new String(encryptToBytes(message, addLengthHeader));
  }

  public static byte[] encryptToBytes(String message, boolean addLengthHeader) {
    return encryptToBytes(message.getBytes(), addLengthHeader);
  }

  public static byte[] encryptToBytes(byte[] message, boolean addLengthHeader) {
    byte[] enc = encryptMessage(message);
    if (addLengthHeader) enc = addHeaderToMessage(enc);

    return enc;
  }

  private static byte[] addHeaderToMessage(byte[] message) {
    return ByteBuffer.allocate(message.length + Integer.BYTES).putInt(message.length).put(message).array();
  }

  private static byte[] encryptMessage(byte[] message) {
    ByteBuffer buffer = ByteBuffer.allocate(message.length);

    int key = START_KEY;
    for (byte value : message) {
      byte b = (byte) (key ^ value);
      key = b;
      buffer.put(b);
    }

    return buffer.array();
  }

  public static String decryptToString(byte[] message) {
    return new String(decryptMessage(message));
  }

  private static byte[] decryptMessage(byte[] message) {
    ByteBuffer buffer = ByteBuffer.allocate(message.length);

    int key = START_KEY;
    for (byte value : message) {
      byte b = (byte) (key ^ value);
      key = value;
      buffer.put(b);
    }

    return buffer.array();
  }

}
