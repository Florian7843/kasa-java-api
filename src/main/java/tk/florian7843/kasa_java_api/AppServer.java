package tk.florian7843.kasa_java_api;

public enum AppServer {

  DEFAULT("https://wap.tplinkcloud.com"),
  US("https://use1-wap.tplinkcloud.com"),
  APAC("https://aps1-wap.tplinkcloud.com"),
  EUROPE("https://eu-wap.tplinkcloud.com");

  private final String url;

  public String getUrl() {
    return url;
  }

  AppServer(String url) {
    this.url = url;
  }
}
